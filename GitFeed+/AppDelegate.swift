//
//  AppDelegate.swift
//  GitFeed+
//
//  Created by Tobias Ostner on 06.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions options: [UIApplicationLaunchOptionsKey: Any]?)
      -> Bool
    {
        let window = UIWindow()
        Application.shared.configureMainInterface(in: window)
        self.window = window
        window.makeKeyAndVisible()
        return true
    }

}

