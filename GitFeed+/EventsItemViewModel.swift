//
//  EventsItemViewModel.swift
//  GitFeed+
//
//  Created by Tobias Ostner on 09.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain

final class EventsItemViewModel {

    let name: String
    let repo: String
    let action: String

    init(_ event: Domain.Event) {
        self.name = event.name
        self.repo = event.repo
        self.action = event.action
    }
}
