//
//  Application.swift
//  GitFeed+
//
//  Created by Tobias Ostner on 09.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain
import NetworkPlatform

final class Application {
    static let shared = Application()

    private let networkUseCaseProvider: NetworkPlatform.UseCaseProvider

    private init() {
        self.networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
    }

    func configureMainInterface(in window: UIWindow) {
        let navigationController = UINavigationController()
        let navigator = DefaultEventsNavigator(services: networkUseCaseProvider, navigationController: navigationController)
        navigator.toEvents()
        window.backgroundColor = .red
        window.rootViewController = navigationController
    }
}
