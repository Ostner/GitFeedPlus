//
//  ActivityIndicator.swift
//  GitFeed+
//
//  Created by Tobias Ostner on 10.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class ActivityIndicator: SharedSequenceConvertibleType {

    typealias E = Bool
    typealias SharingStrategy = DriverSharingStrategy

    private let lock = NSRecursiveLock()
    private let variable = Variable(false)
    private let loading: SharedSequence<DriverSharingStrategy, Bool>

    init() {
        self.loading = variable.asDriver().distinctUntilChanged()
    }

    func asSharedSequence() -> SharedSequence<DriverSharingStrategy, Bool> {
        return loading
    }

    fileprivate func trackActivityOfObservable<O: ObservableConvertibleType>(_ source: O) -> Observable<O.E> {
        return source.asObservable()
          .do(onNext: { _ in self.sendStopLoading() },
              onError: { _ in self.sendStopLoading() },
              onCompleted: { self.sendStopLoading() },
              onSubscribe: subscribed)
    }

    private func subscribed() {
        lock.lock()
        variable.value = true
        lock.unlock()
    }

    private func sendStopLoading() {
        lock.lock()
        variable.value = false
        lock.unlock()
    }
}

extension ObservableConvertibleType {
    func trackActivity(_ activityIndicator: ActivityIndicator) -> Observable<E> {
        return activityIndicator.trackActivityOfObservable(self)
    }
}
