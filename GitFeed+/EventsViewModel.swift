//
//  EventsViewModel.swift
//  GitFeed+
//
//  Created by Tobias Ostner on 09.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain
import RxSwift
import RxCocoa

final class EventsViewModel: ViewModelType {

    struct Input {
        let trigger: Driver<Void>
    }

    struct Output {
        let events: Driver<[EventsItemViewModel]>
        let fetching: Driver<Bool>
    }

    private let useCase: EventsUseCase

    private let navigator: EventsNavigator

    init(useCase: EventsUseCase, navigator: EventsNavigator) {
        self.useCase = useCase
        self.navigator = navigator
    }

    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let events = input.trigger.flatMapLatest {
            return self.useCase
              .events()
              .trackActivity(activityIndicator)
              .asDriver(onErrorJustReturn: [])
              .map { $0.map(EventsItemViewModel.init) }
        }

        let fetching = activityIndicator.asDriver()
        return Output(events: events, fetching: fetching)
    }
}
