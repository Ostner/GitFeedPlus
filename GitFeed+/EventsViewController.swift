//
//  EventsViewController.swift
//  GitFeed+
//
//  Created by Tobias Ostner on 09.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class EventsViewController: UIViewController {

    private let disposeBag = DisposeBag()

    private var layout = UICollectionViewFlowLayout()

    var viewModel: EventsViewModel!

    var collectionView: UICollectionView {
        return view as! UICollectionView
    }

    override func loadView() {
        view = UICollectionView(frame: .zero, collectionViewLayout: layout)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        bindViewModel()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        layout.itemSize = CGSize(width: view.bounds.width, height: view.bounds.height*0.15)
    }

    func bindViewModel() {
        let viewWillAppear = rx
          .sentMessage(#selector(UIViewController.viewWillAppear(_:)))
          .map { _ in }
          .asDriver(onErrorJustReturn: ())
        let pullToRefresh = collectionView
          .refreshControl!.rx
          .controlEvent(.valueChanged)
          .asDriver()

        let input = EventsViewModel.Input(trigger: Driver.merge(viewWillAppear, pullToRefresh))
        let output = viewModel.transform(input: input)

        output.events
          .debug("events")
          .drive(collectionView.rx.items(cellIdentifier: "Cell", cellType: EventsCell.self)) {
              cv, model, cell in
              cell.bind(model)
          }
          .disposed(by: disposeBag)

        output.fetching
          .debug("fetching")
          .drive(collectionView.refreshControl!.rx.isRefreshing)
          .disposed(by: disposeBag)

    }

    private func configureCollectionView() {
        collectionView.backgroundColor = .white
        collectionView.refreshControl = UIRefreshControl()
        collectionView.register(EventsCell.self, forCellWithReuseIdentifier: "Cell")
    }

}
