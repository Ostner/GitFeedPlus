//
//  EventsNavigator.swift
//  GitFeed+
//
//  Created by Tobias Ostner on 09.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import Domain

protocol EventsNavigator {}

class DefaultEventsNavigator: EventsNavigator {
    private let navigationController: UINavigationController
    private let services: UseCaseProvider

    init(services: UseCaseProvider, navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.services = services
    }
    
    func toEvents() {
        let vc = EventsViewController()
        vc.viewModel = EventsViewModel(useCase: services.makeEventsUseCase(), navigator: self)
        navigationController.pushViewController(vc, animated: true)
    }
}
