//
//  EventsCell.swift
//  GitFeed+
//
//  Created by Tobias Ostner on 09.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

final class EventsCell: UICollectionViewCell {

    let name: UILabel = {
        let name = UILabel()
        name.translatesAutoresizingMaskIntoConstraints = false
        name.font = .preferredFont(forTextStyle: .title2)
        return name
    }()

    let repo: UILabel = {
        let repo = UILabel()
        repo.translatesAutoresizingMaskIntoConstraints = false
        repo.font = .preferredFont(forTextStyle: .body)
        return repo
    }()

    let action: UILabel = {
        let action = UILabel()
        action.translatesAutoresizingMaskIntoConstraints = false
        action.font = .preferredFont(forTextStyle: .body)
        return action
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        repo.translatesAutoresizingMaskIntoConstraints = false
        action.translatesAutoresizingMaskIntoConstraints = false

        let infoStack = UIStackView(arrangedSubviews: [name, repo, action])
        infoStack.axis = .vertical
        infoStack.distribution = .fillProportionally
        infoStack.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(infoStack)
        infoStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15).isActive = true
        infoStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15).isActive = true
        infoStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15).isActive = true
        infoStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func bind(_ viewModel: EventsItemViewModel) {
        name.text = viewModel.name
        repo.text = viewModel.repo
        action.text = viewModel.action
    }
}
