//
//  EventsUseCase.swift
//  NetworkPlatform
//
//  Created by Tobias Ostner on 09.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain
import Moya
import RxSwift

final class EventsUseCase: Domain.EventsUseCase {
    
    private let provider = MoyaProvider<GithubService>()

    func events() -> Observable<[Domain.Event]> {
        return provider
          .rx.request(.events)
          .filterSuccessfulStatusCodes()
          .map([Domain.Event].self)
          .asObservable()
    }

}
