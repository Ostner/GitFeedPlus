//
//  Event+Decodable.swift
//  NetworkPlatform
//
//  Created by Tobias Ostner on 09.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain

extension Event: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case actor
        case repo
        case action = "type"
    }
    
    enum ActorCodingKeys: String, CodingKey {
        case name = "login"
        case imageURL = "avatar_url"
    }
    
    enum RepoCodingKeys: String, CodingKey {
        case name
    }
    
    enum PayloadCodingKeys: String, CodingKey {
        case action
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let actor = try container.nestedContainer(keyedBy: ActorCodingKeys.self, forKey: .actor)
        let name = try actor.decode(String.self, forKey: .name)
        let imageURL = try actor.decode(URL.self, forKey: .imageURL)
        
        let repository = try container.nestedContainer(keyedBy: RepoCodingKeys.self, forKey: .repo)
        let repo = try repository.decode(String.self, forKey: .name)
        
        let action = try container.decode(String.self, forKey: .action)
        
        self.name = name
        self.imageURL = imageURL
        self.repo = repo
        self.action = action
    }
}
