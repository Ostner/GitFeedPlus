//
//  UseCaseProvider.swift
//  NetworkPlatform
//
//  Created by Tobias Ostner on 09.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain

public final class UseCaseProvider: Domain.UseCaseProvider {
    
    public func makeEventsUseCase() -> Domain.EventsUseCase {
        return EventsUseCase()
    }
    
    public init() {}
}
