//
//  NetworkPlatform.h
//  NetworkPlatform
//
//  Created by Tobias Ostner on 08.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NetworkPlatform.
FOUNDATION_EXPORT double NetworkPlatformVersionNumber;

//! Project version string for NetworkPlatform.
FOUNDATION_EXPORT const unsigned char NetworkPlatformVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NetworkPlatform/PublicHeader.h>


