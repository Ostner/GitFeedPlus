//
//  GithubService.swift
//  NetworkPlatform
//
//  Created by Tobias Ostner on 09.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import Moya

enum GithubService {
    case events
}

extension GithubService: TargetType {

    var baseURL: URL {
        return URL(string: "https://api.github.com/repos/ReactiveX/RxSwift")!
    }

    var path: String {
        switch self {
        case .events: return "/events"
        }
    }

    var method: Moya.Method {
        return .get
    }

    var task: Task {
        return .requestPlain
    }

    var sampleData: Data {
        return Data()
    }

    var headers: [String: String]? {
        return nil
    }
}
