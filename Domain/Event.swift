//
//  Event.swift
//  Domain
//
//  Created by Tobias Ostner on 06.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

public struct Event {
    public let repo: String
    public let name: String
    public let imageURL: URL
    public let action: String
}
