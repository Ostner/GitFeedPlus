//
//  UseCaseProvider.swift
//  Domain
//
//  Created by Tobias Ostner on 06.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

public protocol UseCaseProvider {
    func makeEventsUseCase() -> EventsUseCase
}
